# coding:utf8

b = [(1, 2.3), (50, 1.2), (20, 2.0)]

def sort_by_qty(price_list):
    '''
    根据qty去进行排序
    @sample:
        price_list: [(1, 2.3), (50, 1.2), (20, 2.0)]
        return: [(1, 2.3), (20, 2.0), (50, 1.2)]
    '''
    return int(price_list[0])

new_b = sorted(b, key=sort_by_qty)
print b
print new_b
