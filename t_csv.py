# coding:utf8
# *******************************************
# author: chj
# create: 2015-06-16 10:47:23
# note: 简单测试csv的reader和writer
# *******************************************

import csv

filename = 'test.csv'

def write(filename=filename):
    # delimiter default: ","
    writer = csv.writer(file(filename, 'wb'), delimiter="|")
    writer.writerow(["chj", "is|a", "male"])
    writer.writerow(["hzz", "is|a", "female"])

def read(filename=filename):
    reader = csv.reader(file(filename, 'rb'), delimiter="|")
    for each_line in reader:
        yield each_line

if __name__ == '__main__':
    write()
    for each_line in read():
        print each_line
