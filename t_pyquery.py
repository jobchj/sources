# coding:utf8
# *******************************************
# author: chj
# create: 2015-06-17 10:38:03
# note: 尝试pyquery解析html
# *******************************************

from pyquery import PyQuery as pyq

html = """
<html>
<head>
    <title> this is title </title>
</head>
<body>
    <p id="hi">hello</p>
    <h1 class="world">world</h1>

    <ul id="l1">
        <li>list1</li>
        <li>list2</li>
    </ul>

    <ul id="l2">
        <li>list3</li>
        <li>list4</li>
    </ul>
</body>
</html>
"""

jq = pyq(html)
print(jq('title'))
print(jq('#hi'))
print(jq('.world'))
print(jq('.world').text().strip())

lis = jq("#l1 li")
for li in lis:
    print jq(li).text()
