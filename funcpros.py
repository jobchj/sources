# coding:utf8
# *******************************************
# author: chj
# create: 2015-06-16 10:49:54
# note: 测试函数式编程
# *******************************************

def t_lambda():
    """
    构造一个简单函数时, 可直接用lambda

    In [1]: a = lambda x, y: x * y

    In [2]: a(5, 8)
    Out[2]: 40
    """
    a = lambda x, y: x * y
    print a(5, 8)
    return a

def t_map():
    """
    对 一个序列 中的每个元素 都执行一次 函数, 所得到的每个结果构成一个新序列

    In [1]: l = [1, 2, 3, 4]

    In [2]: map(lambda x: x * 2, l)
    Out[2]: [2, 4, 6, 8]
    """
    l = [1, 2, 3, 4]
    new_l = map(lambda x: x * 2, l)
    print new_l

def t_reduce():
    """
    对 一个序列, 中的每个元素 迭代调用函数, 返回一个结果

    In [3]: l = [1, 2, 3, 4]
    In [5]: reduce(lambda x, y: x * y, l)
    Out[5]: 24
    """
    a = t_lambda()
    l = [1, 2, 3, 4]
    print reduce(a, l)

def t_filter():
    """
    对 序列 中每个元素执行一次函数, 把返回值为True的返回构成一个新序列

    In [6]: l = [1, 2, 3, 4, 5, 6, 7, 8]

    In [7]: filter(lambda x: x % 2, l)
    Out[7]: [1, 3, 5, 7]
    """
    l = [1, 2, 3, 4, 5, 6, 7, 8]
    print filter(lambda x: x % 2, l)

def t_enum():
    a = ['a', 'b', 'c', 'd']
    b = enumerate(a)
    for index, value in b:
        print('{}:{}'.format(index, value))

if __name__ == '__main__':
    t_lambda()
    t_map()
    t_reduce()
    t_filter()
    t_enum()
