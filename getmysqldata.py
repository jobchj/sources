# coding:utf8
# *******************************************
# author: chj
# create: 2015-06-24 11:11:30
# note: 从mysql读取数据写入到csv文件
# *******************************************

import sys
reload(sys)
sys.setdefaultencoding('utf8')

import csv

import MySQLdb

class My_Mysql(object):
    def __init__(self, host, username, password, db, port=3306, charset="utf8"):
        self.host = host
        self.db = db
        self.__conn = MySQLdb.connect(host, username, password, db,
                                      port, charset=charset)
        self.__cursor = self.__conn.cursor(MySQLdb.cursors.DictCursor)

    def __del__(self):
        """
        销毁
        """
        print('program exit, close connection')
        self.__cursor.close()
        self.__conn.close()

    def __str__(self):
        return 'host: %s, db: %s' % (self.host, self.db)

    def all_data(self):
        sql = """select * from product"""
        num = self.__cursor.execute(sql)
        if num > 0:
            all_data = self.__cursor.fetchall()
            for each in all_data:
                yield each
        else:
            return

if __name__ == '__main__':
    db_dic = {
        'host': '127.0.0.1',
        'username': 'root',
        'password': 'a',
        'db': 'HongKong',
    }
    mysql = My_Mysql(**db_dic)
    writer = csv.writer(file("rs.csv", 'wb'))

    writer.writerow(['sup_partno', 'partno', 'mfr', 'price', 'url'])

    datas = mysql.all_data()
    for each in datas:
        writer.writerow([each['rs_partno'], each['mfr_partno'], each['mfr'],
                         each['price_dict'], each['url']])
