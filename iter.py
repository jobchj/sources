# coding:utf8
from itertools import combinations, product

n = 4
d = 3

def visit(*indices):
    print indices

for i in range(n):
    for j in range(n):
        for k in range(n):
            visit(i, j, k)

for indices in product(*([range(n)]* d)):
    visit(*indices)
