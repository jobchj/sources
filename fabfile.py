# coding:utf8
# *******************************************
# author: chj
# create: 2015-06-16 14:59:57
# note: http://docs.fabfile.org/en/1.10/
# *******************************************

from fabric.api import run, local
from fabric.colors import red

def ls():
    local('ls -l')

def host_type():
    run('uname -s')

def hello():
    print 'hello world'

def hello_with_args(name="world"):
    print red('usage: fab hello_with_args:name=kim')
    print red('usage: fab hello_with_args:kim')
    print 'hello %s' % name
