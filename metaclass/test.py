# coding:utf8
# *******************************************
# author: chj
# create: 2015-06-18 10:16:17
# note: http://blog.ionelmc.ro/2015/02/09/understanding-python-metaclasses/
# *******************************************

def my_metaclass(cls_name, parents, attributes):
    print 'in meataclass, creating the class'
    return type(cls_name, parents, attributes)

def my_class_decorator(class_):
    print 'in decorator, chance to modify the class'
    return class_

@my_class_decorator
class C(object):
    __metaclass__ = my_metaclass

    def __init__(self):
        print 'creating object'
