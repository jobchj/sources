# coding:utf8
# *******************************************
# author: chj
# create: 2015-06-18 15:41:32
# note: http://sahandsaba.com/python-decorators.html
# *******************************************
import time

def logged(time_format):
    def decorator(func):
        def decorator_func(*args, **kwargs):
            print args
            print kwargs
            print func.__name__
            print "Running '%s' on %s " % (func.__name__, time.strftime(time_format))

            start = time.time()
            result = func(*args, **kwargs)
            end = time.time()
            print "Finished '%s', execution time = %0.3fs " % (func.__name__, end - start)

            return result
        decorator_func.__name__ = func.__name__
        return decorator_func
    return decorator

@logged("%b %d %Y %H:%M:%S")
def add1(x, y):
    time.sleep(2)
    return x + y

print add1(1, 2)
