# coding:utf8

import gevent
from gevent import monkey
# 这句话一定要放在其他import前面, 否则会抛异常
monkey.patch_all()

import requests
each_count = 10
# roch url 保密
url = 'http://api.rocelec.com/parts/*/500/%s/?key=XXXXXXX'


def get_total_page():
    r = requests.get(url % 1)
    s = r.json()
    return s['page_count']

def get_from_api(start_page):
    end_page = start_page + each_count
    page_now = start_page
    while page_now < end_page:
        print page_now
        page_now += 1

if __name__ == '__main__':
    total_page = get_total_page()
    process_count = total_page / 100 + 1
    jobs = []
    for each_digit in range(process_count):
        start_page = 100 * each_digit + 1
        jobs.append(gevent.spawn(get_from_api, start_page))
    gevent.wait(jobs)
