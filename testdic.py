# coding:utf8
# *******************************************
# author: chj
# create: 2015-06-16 17:05:43
# note: 三种方式
# *******************************************

import time

start1 = time.time()
dic1 = {}
for i in xrange(16000):
    s = 'abcd%s' % (i % 400)
    if s not in dic1:
        dic1[s] = 1
    else:
        dic1[s] += 1
print(dic1)
end1 = time.time()
print(end1 - start1)

start2 = time.time()
dic2 = {}
for i in xrange(16000):
    s = 'abcd%s' % (i % 400)
    try:
        dic2[s] += 1
    except KeyError:
        dic2[s] = 1
print(dic2)
end2 = time.time()
print(end2 - start2)


start3 = time.time()
dic3 = {}
for i in xrange(16000):
    s = 'abcd%s' % (i % 400)
    dic3[s] = dic3.get(s, 0) + 1
print(dic3)
end3 = time.time()
print(end3 - start3)
