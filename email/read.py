# coding:utf8
# *******************************************
# author: chj
# create: 2015-06-16 15:04:32
# note: 使用poplib收取邮件
# *******************************************

import poplib
import cStringIO as StringIO
import email
import base64, os
import time

USER = 'stock@etime.net.cn'
PASSWD = 'password'
yesterday = time.strftime("%m%d%Y", time.localtime(time.time() - 24 * 60 * 60))
filename = 'AMPT_Stock1_%s.txt' % yesterday
SERVER = 'mail.etime.net.cn'

if os.path.isfile(filename):
    print 'file is exist..'
    exit(0)

M = poplib.POP3(SERVER)
M.user(USER)
M.pass_(base64.b64decode(PASSWD))

num = len(M.list()[1])

for i in xrange(num, 0, -1):
    m = M.retr(i)

    flag = False

    buf = StringIO.StringIO()
    for j in m[1]:
        print>>buf, j
    buf.seek(0)
    msg = email.message_from_file(buf)
    subject = msg.get('subject')
    # 筛选主题为 Advanced MP Stock Update 的邮件
    if subject == 'Advanced MP Stock Update':
        for par in msg.walk():
            if not par.is_multipart():
                name = par.get_param('name')
                if name and name == filename:
                    print 'filename:%s' % name
                    flag = True
                    with open(filename, 'wb') as fp:
                        fp.write(par.get_payload(decode=type))
        if flag:
            break
