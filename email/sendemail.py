# coding=utf-8
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText

# python 2.3.*: email.Utils email.Encoders
from email.utils import COMMASPACE, formatdate
from email import encoders

import os

DIRNAME = os.path.dirname(os.path.abspath(__file__))

def send_mail(server, fro, to, subject, text, files=[]):
    assert type(server) == dict
    assert type(to) == list
    assert type(files) == list

    msg = MIMEMultipart()
    msg['From'] = fro
    msg['Subject'] = subject
    msg['To'] = COMMASPACE.join(to)
    msg['Date'] = formatdate(localtime=True)
    msg.attach(MIMEText(text, _subtype='html', _charset='utf-8'))

    for file in files:
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(open(file, 'ra').read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(file))
        msg.attach(part)

    '''
    # 发送图片
    file1 = DIRNAME+"/yuejie/logo.jpg"
    file2 = DIRNAME+"/yuejie/tel.jpg"
    image = MIMEImage(open(file1,'rb').read())
    image2 = MIMEImage(open(file2,'rb').read())
    image.add_header('Content-ID','logo.jpg')
    image2.add_header('Content-ID','tel.jpg')
    msg.attach(image)
    msg.attach(image2)
    '''

    import smtplib
    smtp = smtplib.SMTP(server['name'])
    smtp.login(server['user'], server['passwd'])
    smtp.sendmail(fro, to, msg.as_string())
    # smtp.close()

if __name__ == '__main__':
    to = ['chenhuanjin@sina.cn']
    subject = 'testsub'
    content = 'testContent'
    from_ = "server@server.com"
    text = content
    files = []
    server = {'name': 'smtp.server.net',
              'user': "server@server.com",
              'passwd': 'password'}
    send_mail(server, from_, to,
              subject, text, files)
