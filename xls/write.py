# coding:utf8
# *******************************************
# author: chj
# create: 2015-06-16 16:18:27
# note: 修改xls的字体颜色, 查文档查了好久, 特此记录下来
# *******************************************

"""
color index:
0 = Black, 1 = White, 2 = Red, 3 = Green, 4 = Blue, 5 = Yellow, 6 = Magenta,
7 = Cyan, 16 = Maroon, 17 = Dark Green, 18 = Dark Blue,
19 = Dark Yellow , almost brown), 20 = Dark Magenta, 21 = Teal,
22 = Light Gray, 23 = Dark Gray,
"""

import xlwt

book = xlwt.Workbook()
sheet = book.add_sheet(u'change_font_color')

font = xlwt.Font()
font.colour_index = 4
style = xlwt.XFStyle()
style.font = font

sheet.write(0, 0, 'blue font', style)
path = 'blue.xls'
book.save(path)
