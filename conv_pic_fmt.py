# coding:utf8
# *******************************************
# author: chj
# create: 2014-07-21 09:00:32
# note: 很多时候把图片下到本地, 为了节省空间, 会转换格式
# *******************************************

import Image


def conv(filename, fmt='jpg'):
    try:
        im = Image.open(filename)
        tmp = filename.split('.')
        if len(tmp) == 1:
            newfilename = filename + '.' + fmt
        else:
            tmp[-1] = fmt
            newfilename = '.'.join(tmp)
        # 必须, 否则容易抛异常
        im = im.convert('RGB')
        im.save(newfilename)
        return True
    except:
        import traceback
        print traceback.format_exc()
        return False
