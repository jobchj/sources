# coding:utf8
import sys
import getopt

# options, args = getopt.getopt(sys.argv[1:], "hp:i:")
#
# print options
# print args
#
# for name, value in options:
#     if name in ('-h', '--help'):
#         print 'usage: python opt.py'
#     elif name in ('-i', '--ip'):
#         print 'ip is %s' % value
#     elif name in ('-p', '--port'):
#         print 'port is %s' % value

options, args = getopt.getopt(sys.argv[1:], "hp:i:", ["help", "ip=", "port="])

print options
print args

for name, value in options:
    if name in ('-h', '--help'):
        print 'usage: python opt.py'
    elif name in ('-i', '--ip'):
        print type(value)
        print 'ip is %s' % value
    elif name in ('-p', '--port'):
        print 'port is %s' % value
