# coding:utf8
from multiprocessing import Process
import time
from multiprocessing import Manager

m = Manager()
is_finished = m.Value('flag', False)
q = m.Queue()

def p():
    "put something random here"
    for i in range(10):
        q.put(i, block=True)
    # q.task_done()
    is_finished.value = True
    exit(0)

def g(id_g):
    while not (is_finished.value and q.empty()):
        print '---is_finished: %s' % is_finished.value
        print '----q.empty(): %s' % q.empty()
        try:
            name = q.get(block=True, timeout=1)
            q.task_done()
            print '%s:%s' % (id_g, name)
            time.sleep(1)
        except:
            continue

if __name__ == '__main__':
    pss = []
    pss.append(Process(target=p))
    for i in range(4):
        pss.append(Process(target=g, args=(i,)))
    for i in pss:
        i.start()
    for i in pss:
        i.join()

    # pool = multiprocessing.Pool(processes=4)
    # pool.apply_async(p)
    # for i in range(4):
    #     pool.apply_async(g, (i,))
    # print 'put finished, now start..'
    # pool.close()
    # pool.join()
    # print 'done'
